# CiscoSEC

## Installation

### Required software:

- GNS3 with a GUI
- Docker

#### Installation

Necessary docker images:
- Ubuntu latest: `gns3/ubuntu` , installed early 2019, which should work as a GNS3 appliance, otherwise install with `docker pull ubuntu`
- TFTP SERVER, which isn't used successfuly, but remains in some topologies `sparklyballs/tftp-server`
- SNMP, another legacy topology unit: `elcolio/net-snmp`
- Syslog, `rsyslog/syslog_appliance_alpine`

System images:
- c7200-advsecurityk9-mz.151-4.M9.image
- c3725-adventerprisek9-mz.124-15.T14.image
- Cisco IOSvL2 (for ARPAttacks2 only)


## Contents

### AAA

    - Create AAA server and enable it(not done, where is an AAA docker??)

### Access
    - Secure console, vty and aux access methods(done)

### SSH
    - Setup and establish an SSH connection between an Ubuntu docker image(done)
    - TODO: Remove telnet capabilities

### BruteForce
    - Secure the perimeter in case of a brute force attack
    - Long password, log attempts, block consecutive unsuccessful attempts, delay login

### Syslog
    - Create and establish a SysLog server
    - Ubuntu SysLog server and Cisco Router sending syslog data to it
    - TODO: Make Ubuntu server docker show SysLog data!

### SNMP
    - Create and establish an SNMP server
    - Alpine docker image running snmpd receiving snmp packets
    - Currently manually sending GET requests to a specified location
    - TODO: display SNMP data sent from the router!
### TFTP
    - Establish a TFTP server using the GNS3 appliance
    - Send the startup config to it using copy
### AccessLists
    - Create an extended access list and the regular one
### Zone-Based-Firewall
    - Create a Zone-Based-Firewall, using the C7200 advescurity image
### OSP Authentication
    - Establish OSPF authentication between two interfaces using MD5 hashing algorithm
    - Both sides of the communication need to have the same key, as the MD5 hashes, together with the time sent is then compared on both sides and need to match
    - Setup passive interfaces, so we don't have OSPF traffic spreading to networks that do not need this capability
### ARP and DHCP security
    - Establish DHCP snooping
    - Establish Dynamic Arp Inspection and ARP snooping
    - Make them work together, to prevent ARP spoofing, ARP poisoning and rogue DHCP server attacks
    - Turn off option 82
    - TL;DR ARP snooping creates a table, because it listens to DHCP traffic so it knows which client got which address, this is how they work together
### IPSec site to site tunnel
    - Establish an IPSec tunnel between two non-local networks
    - Needs an accesslist as well
### GRE tunnel
    - Establish a GRE tunnel between two physically non-neighbouring networks, to "fool" the router that it is in fact in the same network
